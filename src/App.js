import React from "react";
import {useState} from 'react'
/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
const [count, Setcount] = useState(0)

function handleIncrement() {
  Setcount(c => c + 1)
}

function handleDecrement() {
  Setcount( c => c - 1)
}

let text = POOP_TEXT;
  // let count = 0;
  if (count === 0) {
    text = POOP_TEXT
  } else if (count % 3 === 0 && count % 5 === 0) {
     text = COUNTER_TEXT
  } else if (count % 5 === 0) {
    text = FOO_TEXT
  } else if (count % 3 === 0) {
    text = COUNTER_TEXT
  } else if (count % 3 !== 0 && count % 5 !== 0)
  text = POOP_TEXT

  return (
    <>
    <div style={styles.app}>
      <h1>{count}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={handleIncrement}>Increment</button>
        <button onClick={handleDecrement}>Decrement</button>
      </div>
    </div>
    </>
  );
}




const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
